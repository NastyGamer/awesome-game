package awgame.game.Util;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class VectorConvert {

	public static Vector3 fromVector2(Vector2 vector) {
		return new Vector3(vector.x, vector.y, 0);
	}

}
