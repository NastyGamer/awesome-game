package awgame.game;

import awgame.game.Util.VectorConvert;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;

public class Player extends Entity {

	private OrthographicCamera camera;
	private Texture texture;
	public int width, height;

	public Player(Texture spritesheet, int tilewidth, int tileheight, Activity[] activities) {
		super(spritesheet, tilewidth, tileheight, activities);
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.zoom = 0.2f;
		camera.position.set(VectorConvert.fromVector2(location));
		camera.update();
		width = tilewidth;
		height = tileheight;
	}

	OrthographicCamera getCamera() {
		return camera;
	}

	void moveCameraToPlayer() {
		camera.position.set(VectorConvert.fromVector2(location));
	}
}
