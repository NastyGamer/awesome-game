package awgame.game;

import com.badlogic.gdx.graphics.Texture;

public class Monster extends Entity {

	public Monster(Texture spritesheet, int tilewidth, int tileheight, Activity[] activities) {
		super(spritesheet, tilewidth, tileheight, activities);
	}
}
