package awgame.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashMap;

public class Entity {

	public enum DIRECTION {LEFT, RIGHT}

	public enum Activity {IDLE, MOVE, ATTACK, DIE}

	DIRECTION currentDirection = DIRECTION.RIGHT;
	Vector2 location = new Vector2(50, 50);

	private HashMap<Activity, Animation<TextureRegion>> animations = new HashMap<Activity, Animation<TextureRegion>>();

	Entity(Texture spritesheet, int tilewidth, int tileheight, Activity[] activities) {
		TextureRegion[][] temp = TextureRegion.split(spritesheet, tilewidth, tileheight);
		for (int i = 0; i < activities.length; i++) {
			ArrayList<TextureRegion> textures = new ArrayList<TextureRegion>();
			for (TextureRegion[] textureRegions : temp) {
				textures.add(textureRegions[i]);
				Animation<TextureRegion> animation = new Animation<TextureRegion>(0.25f, textures.toArray(new TextureRegion[]{}));
				animation.setPlayMode(Animation.PlayMode.LOOP);
				animations.put(activities[i], animation);
			}
		}
	}

	TextureRegion getFrame(float time, Activity activity) {
		System.out.println(currentDirection);
		TextureRegion region = animations.get(activity).getKeyFrame(time, true);
		if (currentDirection == DIRECTION.LEFT && !region.isFlipX()) region.flip(true, false);
		else if (region.isFlipX()) region.flip(false, false);
		return region;
	}

	void move(Vector2 velocity) {
		location.add(velocity);
	}
}
