package awgame.game.Tiles;

import awgame.game.Entity;
import awgame.game.Monster;
import awgame.game.Player;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;

public class World {

	public TileMap map = new TileMap(2, 32, 32, 16);
	public Monster monster;
	public Player player;

	public World() {
		Candlestick candlestick = new Candlestick();
		candlestick.setAnimation(new Texture("Candlestick.png"), 16, 0.4f);
		candlestick.setAnimationPlaymode(Animation.PlayMode.LOOP_PINGPONG);
		candlestick.setCollide(true);
		Floor floor = new Floor(new Texture("Floor.png"));
		map.fillLayer(TileMap.LAYER.BACKGROUND, floor);
		map.setTile(candlestick, TileMap.LAYER.OBJECT, 0, 0);
		Torch torch = new Torch();
		torch.setAnimation(new Texture("Torch.png"), 16, 0.5f);
		torch.setAnimationPlaymode(Animation.PlayMode.LOOP_PINGPONG);
		torch.setCollide(true);
		map.setTile(torch, TileMap.LAYER.OBJECT, 1, 0);
		monster = new Monster(new Texture("Monster.png"), 32, 32, new Entity.Activity[]{Entity.Activity.IDLE, Entity.Activity.MOVE});
		player = new Player(new Texture("Player.png"), 16, 28, new Entity.Activity[]{Entity.Activity.IDLE, Entity.Activity.MOVE, Entity.Activity.ATTACK});
	}

	public boolean collides(Vector2 location, int width, int height) {
		return map.collides(location, width, height);
	}
}
