package awgame.game.Tiles;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

public class TileMap {

	enum LAYER {BACKGROUND, OBJECT}

	private TileMapLayer background;
	private TileMapLayer objects;
	ArrayList<Rectangle> collisions = new ArrayList<Rectangle>();

	private int tilesize;

	TileMap(int layers, int width, int height, int tilesize) {
		background = new TileMapLayer(width, height);
		objects = new TileMapLayer(width, height);
		this.tilesize = tilesize;
	}

	void setTile(Tile tile, LAYER layer, int x, int y) {
		if (layer == LAYER.BACKGROUND) background.setTile(tile, x, y);
		else objects.setTile(tile, x, y);
		if (tile.collide) collisions.add(new Rectangle(x * tilesize, y * tilesize, tilesize, tilesize));
	}

	public Tile getTile(LAYER layer, int x, int y) {
		if (layer == LAYER.BACKGROUND) return background.getTile(x, y);
		return objects.getTile(x, y);
	}

	private boolean isAnimated(Tile tile) {
		return tile instanceof AnimatedTile;
	}

	void fillLayer(LAYER layer, Tile tile) {
		for (int x = 0; x < background.getSize(); x++) {
			for (int y = 0; y < background.getSize(); y++) {
				if (layer == LAYER.BACKGROUND) background.setTile(tile, x, y);
				else objects.setTile(tile, x, y);
			}
		}
	}

	public void requestPaint(SpriteBatch batch, float elapsed) {
		batch.begin();
		for (int x = 0; x < background.getSize(); x++) {
			for (int y = 0; y < background.getSize(); y++) {
				batch.draw(((StaticTile) background.getTile(x, y)).getTexture(), x * tilesize, y * tilesize);
			}
		}
		for (int x = 0; x < objects.getSize(); x++) {
			for (int y = 0; y < objects.getSize(); y++) {
				try {
					if (isAnimated(objects.getTile(x, y)))
						batch.draw(((AnimatedTile) objects.getTile(x, y)).getFrame(elapsed), x * tilesize, y * tilesize);
					else batch.draw(((StaticTile) objects.getTile(x, y)).getTexture(), x * tilesize, y * tilesize);
				} catch (NullPointerException ignored) {
				}
			}
		}
		batch.end();
	}

	boolean collides(Vector2 p, int width, int height) {
		for (Rectangle r : collisions) if (r.contains(p.cpy().add(width, height))) return true;
		return false;
	}

	private class TileMapLayer {

		private Tile[][] tiles;

		private TileMapLayer(int width, int height) {
			tiles = new Tile[width][height];
		}

		private void setTile(Tile tile, int x, int y) {
			tiles[x][y] = tile;
		}

		private Tile getTile(int x, int y) {
			return tiles[x][y];
		}

		int getSize() {
			return tiles.length;
		}
	}
}
