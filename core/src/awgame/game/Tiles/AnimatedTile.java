package awgame.game.Tiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimatedTile extends Tile {

	private Animation<TextureRegion> animation;

	public void setAnimation(Texture spritesheet, int tilesize, float time) {
		animation = new Animation<TextureRegion>(time, TextureRegion.split(spritesheet, tilesize, tilesize)[0]);
	}

	public void setAnimationFramerate(float time) {
		animation.setFrameDuration(time);
	}

	public void setAnimationPlaymode(Animation.PlayMode mode) {
		animation.setPlayMode(mode);
	}

	public TextureRegion getFrame(float time) {
		return animation.getKeyFrame(time, true);
	}
}
