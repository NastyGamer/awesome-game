package awgame.game.Tiles;

import com.badlogic.gdx.graphics.Texture;

public class StaticTile extends Tile {

	private Texture texture;

	StaticTile(Texture texture) {
		this.texture = texture;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	Texture getTexture() {
		return texture;
	}

}
