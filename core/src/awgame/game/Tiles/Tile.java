package awgame.game.Tiles;

import com.badlogic.gdx.math.Vector2;

public class Tile {

	boolean collide;
	Vector2 position = new Vector2();

	public void setCollide(boolean collide) {
		this.collide = collide;
	}

	public void setPosition(Vector2 position) {
		this.position.set(position);
	}
}
