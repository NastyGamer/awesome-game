package awgame.game;

import awgame.game.Tiles.World;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.awt.*;

public class AwesomeGame extends ApplicationAdapter {

	private World world;
	private SpriteBatch spriteBatch;
	private float timeElapsed = 0;

	@Override
	public void create() {
		world = new World();
		spriteBatch = new SpriteBatch();
		spriteBatch.setProjectionMatrix(world.player.getCamera().combined);
		world.player.getCamera().update();
	}

	@Override
	public void render() {
		processInput();
		clearScreen();
		spriteBatch.setProjectionMatrix(world.player.getCamera().combined);
		world.player.getCamera().update();
		world.map.requestPaint(spriteBatch, timeElapsed);
		spriteBatch.begin();
		spriteBatch.draw(world.monster.getFrame(timeElapsed, Entity.Activity.MOVE), world.monster.location.x, world.monster.location.y);
		spriteBatch.draw(world.player.getFrame(timeElapsed, Entity.Activity.MOVE), world.player.location.x, world.player.location.y);
		spriteBatch.end();
		timeElapsed += Gdx.graphics.getRawDeltaTime();
	}

	@Override
	public void dispose() {
		spriteBatch.dispose();
	}

	@Override
	public void resize(int width, int height) {
		if (Gdx.graphics.getWidth() != Toolkit.getDefaultToolkit().getScreenSize().getWidth())
			Gdx.graphics.setWindowedMode(width, (width / 16) * 9);
	}

	private void processInput() {
		if (Gdx.input.isKeyPressed(Input.Keys.W) && !world.collides(world.player.location.add(0, 2), 16, 28)) {
			world.player.move(new Vector2(0, 2));
		} else if (Gdx.input.isKeyPressed(Input.Keys.S) && !world.collides(world.player.location.add(0, -2), 16, 28)) {
			world.player.move(new Vector2(0, -2));
		}
		if (Gdx.input.isKeyPressed(Input.Keys.A) && !world.collides(world.player.location.add(-2, 0), 16, 28)) {
			world.player.move(new Vector2(-2, 0));
			world.player.currentDirection = Entity.DIRECTION.LEFT;
		} else if (Gdx.input.isKeyPressed(Input.Keys.D) && !world.collides(world.player.location.add(2, 0), 16, 28)) {
			world.player.move(new Vector2(2, 0));
			world.player.currentDirection = Entity.DIRECTION.RIGHT;
		}
		world.player.moveCameraToPlayer();
	}

	private void clearScreen() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}
}
